package kg.urmat.order_lib.annotations;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import kg.urmat.order_lib.annotations.validator.EnumContainsValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = EnumContainsValidator.class)
public @interface EnumContains {
    Class<? extends Enum<?>> enumClass();
    String message() default "must be a valid value";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
