package kg.urmat.order_lib.annotations.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import kg.urmat.order_lib.annotations.EnumContains;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Objects.nonNull;

public class EnumContainsValidator implements ConstraintValidator<EnumContains, Enum<?>> {

    private Set<Enum<?>> acceptedValues;

    @Override
    public void initialize(EnumContains annotation) {
        acceptedValues = Stream.of(annotation.enumClass().getEnumConstants())
                .collect(Collectors.toSet());
    }

    @Override
    public boolean isValid(Enum<?> value, ConstraintValidatorContext constraintValidatorContext) {
        return nonNull(value) && acceptedValues.contains(value);
    }
}
