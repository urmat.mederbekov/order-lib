package kg.urmat.order_lib.util.enums;

public enum RoleCode {
    ADMIN,
    USER
}
