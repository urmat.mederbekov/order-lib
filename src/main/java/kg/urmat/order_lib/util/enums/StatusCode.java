package kg.urmat.order_lib.util.enums;

public enum StatusCode {
    PROCESSING,
    SHIPPING,
    COMPLETED
}
