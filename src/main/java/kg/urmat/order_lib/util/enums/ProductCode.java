package kg.urmat.order_lib.util.enums;

public enum ProductCode {
    PHONE,
    COMPUTER,
    LAPTOP,
    EARPHONE,
    IPAD
}
