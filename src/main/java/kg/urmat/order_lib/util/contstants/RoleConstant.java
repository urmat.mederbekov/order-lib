package kg.urmat.order_lib.util.contstants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RoleConstant {

    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";
}