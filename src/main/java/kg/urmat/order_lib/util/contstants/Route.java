package kg.urmat.order_lib.util.contstants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Route {

    private static final String API = "/api";

    public static final String PRODUCTS_API = API + "/products";
    public static final String CARTS_API = API + "/carts";
    public static final String CART_ITEMS_API = API + "/cart-items";
    public static final String ORDERS_API = API + "/orders";
    public static final String ORDER_ITEMS_API = API + "/order-items";
    public static final String USERS_API = API + "/users";
}
