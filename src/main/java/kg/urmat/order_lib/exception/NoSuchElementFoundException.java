package kg.urmat.order_lib.exception;

import java.util.NoSuchElementException;

public class NoSuchElementFoundException extends NoSuchElementException {

    public NoSuchElementFoundException(String message){
        super(message);
    }
}
