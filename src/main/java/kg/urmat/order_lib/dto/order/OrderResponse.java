package kg.urmat.order_lib.dto.order;

import kg.urmat.order_lib.dto.order_item.OrderItemResponse;
import kg.urmat.order_lib.util.enums.StatusCode;
import lombok.Builder;

import java.util.List;

@Builder
public record OrderResponse(
        Long id,
        String userId,
        Double totalCost,
        StatusCode status,
        List<OrderItemResponse> orderItems
){}
