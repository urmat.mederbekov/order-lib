package kg.urmat.order_lib.dto.cart;

import kg.urmat.order_lib.dto.cart_item.CartItemResponse;
import lombok.Builder;

import java.util.List;

@Builder
public record CartResponse(
        Long id,
        String userId,
        List<CartItemResponse> items
){}
