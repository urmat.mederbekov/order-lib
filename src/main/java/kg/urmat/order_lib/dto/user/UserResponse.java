package kg.urmat.order_lib.dto.user;

import kg.urmat.order_lib.dto.role.RoleResponse;
import lombok.Builder;

import java.time.LocalDate;
import java.util.Set;

@Builder
public record UserResponse(
        String authId,
        String username,
        String name,
        String surname,
        LocalDate birthDate,
        Set<RoleResponse> roles
){}