package kg.urmat.order_lib.dto.user;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Past;
import kg.urmat.order_lib.util.enums.RoleCode;
import lombok.Builder;

import java.time.LocalDate;
import java.util.Set;

@Builder
public record UserUpdateRequest(
        @NotBlank
        String name,
        @NotBlank
        String surname,
        @Past
        LocalDate birthDate,
        Set<RoleCode> roleCodes
){}