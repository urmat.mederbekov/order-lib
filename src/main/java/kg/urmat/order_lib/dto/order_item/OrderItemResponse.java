package kg.urmat.order_lib.dto.order_item;

import kg.urmat.order_lib.dto.product.ProductResponse;
import lombok.Builder;

@Builder
public record OrderItemResponse(
        Long id,
        Long orderId,
        ProductResponse product,
        int quantity
){}