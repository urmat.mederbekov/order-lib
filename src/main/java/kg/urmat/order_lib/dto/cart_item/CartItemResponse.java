package kg.urmat.order_lib.dto.cart_item;

import kg.urmat.order_lib.dto.product.ProductResponse;
import lombok.Builder;

@Builder
public record CartItemResponse(
        Long id,
        Long cartId,
        int quantity,
        ProductResponse productDto
){}
