package kg.urmat.order_lib.dto.cart_item;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Builder;

@Builder
public record CartItemCreateRequest(
        @Positive
        Long productId,
        @NotNull
        Integer quantity
){}