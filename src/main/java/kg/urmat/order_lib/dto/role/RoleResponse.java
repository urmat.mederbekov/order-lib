package kg.urmat.order_lib.dto.role;

import kg.urmat.order_lib.util.enums.RoleCode;
import lombok.Builder;

@Builder
public record RoleResponse(
        RoleCode code,
        String name
){}