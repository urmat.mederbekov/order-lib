package kg.urmat.order_lib.dto.product;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import kg.urmat.order_lib.annotations.EnumContains;
import kg.urmat.order_lib.util.enums.ProductCode;
import lombok.Builder;

@Builder
public record ProductUpdateRequest(
        @EnumContains(enumClass = ProductCode.class)
        ProductCode code,
        @NotBlank
        String name,
        @NotBlank
        String description,
        @Positive
        Double price
){}